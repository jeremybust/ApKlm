class CreateStatements < ActiveRecord::Migration
  def change
    create_table :statements do |t|
      t.date :date
      t.text :justify
      t.integer :numberskm
      t.integer :coefficienttaxes
      t.string :path
      t.string :shifting
      t.string :departure

      t.timestamps null: false
    end
  end
end
