class AddAttachmentDocumentToStatements < ActiveRecord::Migration
  def self.up
    change_table :statements do |t|
      t.attachment :document
    end
  end

  def self.down
    remove_attachment :statements, :document
  end
end
