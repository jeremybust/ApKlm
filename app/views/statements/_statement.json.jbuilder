json.extract! statement, :id, :date, :justify, :numberskm, :coefficienttaxes, :path, :shifting, :departure, :created_at, :updated_at
json.url statement_url(statement, format: :json)